;; ---------- パッケージシステム ----------
(require 'package)
;; (add-to-list
;;  'package-archives
;;  '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list
 'package-archives
 '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

;; ---------- パッケージの自動インストール ----------
;; (package-refresh-contents)
;; (package-install 'atom-one-dark-theme)
;; (package-install 'smart-shift)
;; (package-install 'mwim)
;; (package-install 'which-key)
;; (package-install 'package-utils)
;; (package-install 'frame-tag)
;; (package-install 'powerline)
;; (package-install 'comment-dwim-2)
;; (package-install 'helm)
;; (package-install 'markdown-mode)
;; (package-install 'minimap)
;; (package-install 'open-junk-file)
;; (package-install 'undo-tree)
;; (package-install 'neotree)
;; (package-install 'tabbar)
;; (package-install 'web-mode)
;; (package-install 'monokai-theme)
;; (package-install 'anzu)
;; (package-install 'auto-complete)

;; ---------- 全体設定 ----------
;; フォントの設定
(set-language-environment "UTF-8")

;; windowsならフォント設定
(when (eq system-type 'windows-nt)
  )

;; バックアップファイル置き場
(setq backup-directory-alist
      (cons (cons ".*" (expand-file-name "~/.emacs.d/backup-list/"))
            backup-directory-alist))

;; 自動保存ファイル置き場
(setq auto-save-file-name-transforms
      '((".*" "~/.emacs.d/auto-save-list/" t)))

;; ロックファイルを作成しない
(setq auto-save-file-name-transforms
      '((".*" "~/.emacs.d/auto-save-list/" t)))


;; ---------- 常駐設定 ----------
;; タイトルバーにフルパスを表示
(setq frame-title-format "%f")

;; モードラインにリージョン内の行数、文字数を表示
(defun count-lines-and-chars ()
  (if mark-active
      (format "%d lines, %d chars "
              (count-lines (region-beginning) (region-end))
              (- (region-end) (region-beginning)))
    ""))
(add-to-list 'default-mode-line-format
             '(:eval (count-lines-and-chars)))


;; ---------- 基本設定 ----------
;; 起動画面、ツールバー、メニューバー、スクロール
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; カーソルの設定
(setq-default cursor-type '(bar . 1))

;; 基本インデント幅
(setq-default c-basic-offset 4)
;; タブ文字の幅
(setq-default tab-width 4)
;; インデントにタブ文字を利用しない
(setq-default indent-tabs-mode nil)
;; 自動インデントの無効化
(electric-indent-mode t)

;; 行番号の表示
(global-linum-mode)
(setq linum-format " %d")

;; 行番号をハイライト
(global-hl-line-mode t)

;; 不可視文字の可視化
(global-whitespace-mode 1)

;; 対応するカッコをハイライト
(show-paren-mode t)

;; マウスホイールによるスクロール行数
(setq mouse-wheel-scroll-amount '(1))

;; スクロール量
(setq scroll-conservatively 10)

;; スクロールマージン
(setq scroll-margin 3)

;; 行間
(setq-default line-spacing 2)


;; ---------- 色 ----------
;; 背景色
;; (set-face-background 'default "#282828")
;; (set-face-foreground 'default "#FFFFFF")
;; monokai-theme
(require 'monokai-theme)
;; (require 'atom-one-dark-theme)

;; 行番号と本体の間の色、行番号の色、現在行の色
;; (fringe-mode '(nil . 0))
(fringe-mode (cons 20 nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fringe ((t (:background "gray20"))))
 '(hl-line ((t (:background "gray20"))))
 '(linum ((t (:inherit (shadow default) :background "gray20"))))
 '(margin ((t (:background "gray20")))))

;; (set-face-background 'region "gray20")

;; 不可視文字の設定
(set-face-foreground 'whitespace-empty "#272822")
(set-face-foreground 'whitespace-newline "gray35")
(set-face-foreground 'whitespace-space "gray35")
(set-face-foreground 'whitespace-tab "gray35")
(set-face-foreground 'whitespace-indentation "gray35")

;; 長すぎる行の文字数設定
(setq whitespace-line-column 2000)

;; ヘッダーラインの枠の削除
(set-face-attribute
  'header-line nil
   :box nil)

;; ---------- キーバインドの設定等 ----------
;; C-hを削除キーに
(define-key key-translation-map (kbd "C-h") (kbd "<DEL>"))

;; TABをそのままTABとして使う
;; (global-set-key (kbd "TAB") '(lambda () (interactive) (insert "\t")))
(global-set-key (kbd "TAB") '(lambda () (interactive) (insert "    ")))

;; ポイント位置にマークを置く
(when (eq system-type 'windows-nt)
  (global-set-key (kbd "C-:") 'set-mark-command))
(when (eq system-type 'darwin)
  (global-set-key (kbd "C-\'") 'set-mark-command))

;; C-TABの設定
;; ウィンドウの切り替え
(global-set-key (kbd "<C-tab>") 'other-window)


;; ---------- 機能等 ----------
;; カーソル行の次の次や前に新しい行を追加
(defun my-insert-newline-and-indent-up()
  (interactive)
  (move-beginning-of-line 2)
  (open-line 1)
  (indent-for-tab-command)
  )
(defun my-insert-newline-and-indent-down()
  (interactive)
  (move-beginning-of-line 1)
  (open-line 1)
  (indent-for-tab-command)
  )
;; C-returnに行の追加コマンド
(global-set-key (kbd "<C-return>") 'my-insert-newline-and-indent-up)
(global-set-key (kbd "<C-S-return>") 'my-insert-newline-and-indent-down)

;; インデントの追加、削除
(defun my_indent_plus ()
  (interactive)
  (save-excursion
      (save-restriction
        (unless (region-active-p)
          (beginning-of-line)
          (set-mark (point))
          (end-of-line))
        (narrow-to-region (region-beginning) (region-end))
        (goto-char (point-min))
        (replace-regexp "^" "    ")
        )))
(defun my_indent_minus ()
  (interactive)
  (save-excursion
      (save-restriction
        (unless (region-active-p)
          (beginning-of-line)
          (set-mark (point))
          (end-of-line))
        (narrow-to-region (region-beginning) (region-end))
        (goto-char (point-min))
        (replace-regexp "^    " "")
        )))
(global-set-key (kbd "C-}") 'my_indent_plus)
(global-set-key (kbd "C-{") 'my_indent_minus)

;; ---------- 言語モード設定 ----------
;; 自分用C設定
(defun my-c-mode-common-conf ()
	;; (global-set-key (kbd "TAB") '(lambda () (interactive) (insert "\t")))
	(global-set-key (kbd "TAB") '(lambda () (interactive) (insert "    ")))
)
;; C言語系のモード全てに適用されるフック
(add-hook 'c-mode-common-hook 'my-c-mode-common-conf)


;; ---------- 以下パッケージ関連 ----------
;; tabbar
(tabbar-mode 1)
;; グループ化しない
(setq tabbar-buffer-groups-function nil)
;; 左側のボタンを消す
(dolist (btn '(tabbar-buffer-home-button
               tabbar-scroll-left-button
               tabbar-scroll-right-button))
  (set btn (cons (cons "" nil)
                 (cons "" nil))))
;; タブセパレータの長さ
(setq tabbar-separator '(0.5))
;; 表示するバッファの設定
(defun my-tabbar-buffer-list ()
  (delq nil
		(mapcar #'(lambda (b)
					(cond
					 ((eq (current-buffer) b) b)
					 ((buffer-file-name b) b)
					 ((char-equal ?\ (aref (buffer-name b) 0)) nil)
					 ((equal "*scratch*" (buffer-name b)) b); *scratch*バッファは表示する
					 ((char-equal ?* (aref (buffer-name b) 0)) nil); それ以外の*で始まるバッファは表示しない
					 ((buffer-live-p b) b)))
				(buffer-list))))
(setq tabbar-buffer-list-function 'my-tabbar-buffer-list)
;; tabbarの色
(set-face-attribute
  'tabbar-default nil
   :background "black"
   :height 1.0)
;; アクティブタブ
(set-face-attribute
  'tabbar-selected nil
   :background "#272822"
   :foreground "gray90"
   :box nil)
;; 非アクティブタブ
(set-face-attribute
  'tabbar-unselected nil
   :background "gray12"
   :foreground "gray70"
   :box nil)
;; 修正中のタブ
(set-face-attribute
   'tabbar-modified nil
   :background "#4d394b"
   :foreground "gray70"
   :box nil)
;; アクティブな修正中タブ
(set-face-attribute
   'tabbar-selected-modified nil
   :background "#4c9689"
   :foreground "gray90"
   :box nil)
;; ボタンの設定
(set-face-attribute
   'tabbar-button nil
   :box nil)
;; 縦の長さ
(set-face-attribute
   'tabbar-separator nil
   :height 2.0)

;; タブの切り替え
(global-set-key (kbd "<C-tab>") 'tabbar-forward-tab)
(global-set-key (kbd "<C-S-tab>") 'tabbar-backward-tab)


;; open-junk-file
(require 'open-junk-file)
(setq open-junk-file-format "~/.emacs.d/junk/%Y-%m%d-%H%M%S.txt")

;; ジャンクファイルの作成
(global-set-key (kbd "C-x j") 'open-junk-file)


;; undo-tree
(require 'undo-tree)
(global-undo-tree-mode t)


;; neotree-toggle
(global-set-key (kbd "C-\\") 'neotree-toggle)


;; minimap
(global-set-key (kbd "C-|") 'minimap-mode)


;; helm
(require 'helm)
(require 'helm-config)
(helm-mode 1)

;; helm用キーバインド
(define-key helm-map (kbd "C-h") 'delete-backward-char)
(define-key helm-find-files-map (kbd "C-h") 'delete-backward-char)
(define-key helm-read-file-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-find-files-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") 'helm-select-action)
(define-key global-map (kbd "C-x b") 'helm-for-files)
(define-key global-map (kbd "C-x C-f") 'helm-find-files)
(define-key global-map (kbd "M-x") 'helm-M-x)
(define-key global-map (kbd "M-y") 'helm-show-kill-ring)


;; web-mode
(when (require 'web-mode nil t)
	(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.ctp\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
	(add-hook 'web-mode-hook
		'(lambda()
			;; (global-set-key (kbd "TAB") '(lambda () (interactive) (insert "\t")))
			(global-set-key (kbd "TAB") '(lambda () (interactive) (insert "    ")))
			)
	)
	;; web-modeの色の微調整
	(set-face-attribute 'web-mode-html-tag-face nil :foreground "#F92672")
    (set-face-attribute 'web-mode-html-attr-name-face nil :foreground "#A6E22E")
    (set-face-attribute 'web-mode-css-selector-face nil :foreground "#66D9EF")
    ;; web-modeの自動閉じタグの設定
    (setq web-mode-auto-close-style nil)
    (setq web-mode-tag-auto-close-style nil)
)


;; comment-dwim-2
(global-set-key (kbd "M-;") 'comment-dwim-2)


;; powerline
(require 'powerline)
;; (powerline-default-theme)
(defun powerline-my-theme ()
  "Setup the my mode-line."
  (interactive)
  (setq powerline-current-separator 'utf-8)
  (setq-default mode-line-format
                '("%e"
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          (face1 (if active 'mode-line-1-fg 'mode-line-2-fg))
                          (face2 (if active 'mode-line-1-arrow 'mode-line-2-arrow))
                          (separator-left (intern (format "powerline-%s-%s"
                                                          (powerline-current-separator)
                                                          (car powerline-default-separator-dir))))
                          (lhs (list (powerline-raw " " face1)
                                     (powerline-major-mode face1)
                                     (powerline-raw " " face1)
                                     (funcall separator-left face1 face2)
                                     (powerline-buffer-id nil )
                                     (powerline-raw " [ ")
                                     (powerline-raw mode-line-mule-info nil)
                                     (powerline-raw "%*" nil)
                                     (powerline-raw " |")
                                     (powerline-process nil)
                                     (powerline-vc)
                                     (powerline-raw " ]")
                                     ))
                          (rhs (list (powerline-raw "%4l" 'l)
                                     (powerline-raw ":" 'l)
                                     (powerline-raw "%2c" 'l)
                                     (powerline-raw " | ")
                                     (powerline-raw "%6p" )
                                     (powerline-raw " ")
                                     )))
                     (concat (powerline-render lhs)
                             (powerline-fill nil (powerline-width rhs))
                             (powerline-render rhs)))))))

(defconst color1 "#3E4451")
(set-face-attribute 'mode-line nil
                    :foreground "#fff"
                    :background color1
                    :bold t
                    :box nil)

(set-face-attribute 'mode-line-inactive nil
                    :foreground "#fff"
                    :background color1
                    :bold t
                    :box nil)

(defun make/set-face (face-name fg-color bg-color weight)
  (make-face face-name)
  (set-face-attribute face-name nil
                      :foreground fg-color :background bg-color :box nil :weight weight))
(make/set-face 'mode-line-1-fg "#282C34" "#EF8300" 'bold)
(make/set-face 'mode-line-2-fg "#AAAAAA" "#2F343D" 'bold)
(make/set-face 'mode-line-1-arrow  "#AAAAAA" "#3E4451" 'bold)
(make/set-face 'mode-line-2-arrow  "#AAAAAA" "#3E4451" 'bold)
(powerline-my-theme)

;; anzu
(global-anzu-mode)


;; frame-tag
;; (frame-tag-mode 1)                      ;なぜか機能しない


;; which-key
(which-key-setup-side-window-bottom)    ;ミニバッファ
;; (which-key-setup-side-window-right)     ;右端
;; (which-key-setup-side-window-right-bottom) ;両方使う

(which-key-mode 1)


;; mwim
(global-set-key (kbd "C-a") 'mwim-beginning-of-code-or-line)
(global-set-key (kbd "C-e") 'mwim-end-of-code-or-line)


;; smart-shift
(global-smart-shift-mode 1)
;;; markがないときはエラーになるバグがあるので修正
(defadvice smart-shift-lines (before mark-fix activate)
  (or (mark) (push-mark nil t)))


;; auto-complete
(global-auto-complete-mode t)
;; (global ac-complete-mode-map "M-TAB" 'ac-next)

;; (define-key ac-complete-mode-map "C-n" 'ac-next)
;; (define-key ac-complete-mode-map "C-p" 'ac-previous)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(minimap-window-location (quote right))
 '(package-selected-packages
   (quote
    (auto-complete atom-one-dark-theme smart-shift mwim which-key package-utils frame-tag powerline comment-dwim-2 helm markdown-mode minimap open-junk-file undo-tree neotree tabbar web-mode monokai-theme))))
