" ---------- カラースキーム ----------
" シンタックスハイライトの設定
syntax on
" カラースキームの設定
colorscheme molokai
let g:molokai_original = 1
let g:rehash256 = 1


" ---------- カーソル関連の設定 ----------
" 現在行のハイライト
set cursorline
" カーソルラインをなくす(これをしないと不可視文字の色がおかしなことになるので仕方ない)
hi clear Cursorline


" ---------- 不可視文字表示の設定 ----------
"改行文字とタブ文字の色設定(NonTextが改行、SpecialKeyがタブ)
hi NonText guibg=NONE guifg=Gray30
hi SpecialKey guibg=NONE guifg=Gray30

