" ---------------------------------------------------
" エンコーディングの設定
" ---------------------------------------------------
" ファイル読み込み時の文字コード設定
set encoding=utf-8
" vim script内でマルチバイト文字を使う場合の設定
scriptencoding utf-8


" ---------------------------------------------------
" Leader>設定
" ---------------------------------------------------
" <Leader>を<space>に割り当てる
let mapleader="\<Space>"


" ---------------------------------------------------
" プラグイン関連
" ---------------------------------------------------
" vim-plug
" プラグイン書き込み後「:PlugInstall」を実行
" プラグインを無効にするときはコメントアウトする
call plug#begin()
" molokai
Plug 'tomasr/molokai'

" ツリービュー機能
Plug 'scrooloose/nerdtree'
" ツリーの表示非表示をタブ間で共有
Plug 'jistr/vim-nerdtree-tabs'
" ファイルに変更が加わった場合、ツリーに変更アイコン表示する
Plug 'Xuyuanp/nerdtree-git-plugin'
" アイコン
" Plug 'ryanoasis/vim-devicons'
" フォント
" Plug 'ryanoasis/nerd-fonts'

" gitファイルに変更が加わった時、行番号の横に差分情報をアイコン表示
Plug 'airblade/vim-gitgutter'

" vimのステータスバーを変更する機能
Plug 'vim-airline/vim-airline'
" gitを利用可能にする。airlineにgitブランチ情報を表示する
Plug 'tpope/vim-fugitive'

" インデントガイド
Plug 'Yggdroot/indentLine'

" コメント/アンコメント
Plug 'tomtom/tcomment_vim'

" オートコンプリート
Plug 'Shougo/neocomplete.vim'

" マルチカーソル
Plug 'terryma/vim-multiple-cursors'

" unite
Plug 'Shougo/unite.vim'
" ctrlp
" Plug 'ctrlpvim/ctrlp.vim'

call plug#end()

" ---------------------------------------------------
" nerdtree設定
" ---------------------------------------------------
" 不可視ファイルを表示する
let NERDTreeShowHidden=1

" ツリーと編集領域を移動する
" nmap <Leader><tab> <C-w>w
" ツリーの表示非表示
nmap <Leader>nt :NERDTreeTabsToggle<CR>

" gvim/macvim起動時にnerdtreeをONにする設定。
" 0:ONにしない 1(default):ON 2:引数にディレクトリが指定されている場合のみONにする
let g:nerdtree_tabs_open_on_gui_startup=0

" ---------------------------------------------------
" vim-airlline設定
" ---------------------------------------------------
" タブラインにも適用する
let g:airline#extensions#tabline#enabled = 1

" ---------------------------------------------------
" vim-fugitive設定
" ---------------------------------------------------
" ブランチ情報を表示する
let g:airline#extensions#branch#enabled=1

" ---------------------------------------------------
" tcomment_vim設定
" ---------------------------------------------------
nmap <Leader>/ :TComment<CR>
vmap <Leader>/ :TComment<CR>


" ---------------------------------------------------
" neocomplete設定
" ---------------------------------------------------
" 自動補完機能を有効にする
let g:neocomplete#enable_at_startup=1

" ---------------------------------------------------
" Unite.vim設定
" ---------------------------------------------------
call unite#custom#source('buffer,file_mru,file_rec,file_rec/async','matchers',['matcher_fuzzy'])

nmap <Leader>ff :Unite file_rec<CR>

" ---------------------------------------------------
" カラースキーム
" ---------------------------------------------------
" シンタックスハイライトの設定
syntax on
" カラースキームの設定
colorscheme molokai
let g:molokai_original = 1
let g:rehash256 = 1


" ---------------------------------------------------
" 文字コードの設定
" ---------------------------------------------------
" 保存時の文字コード
set fileencoding=utf-8
" 読み込み時の文字コードの自動判別順。左側から優先。
set fileencodings=ucs-boms,utf-8,euc-jp,cp932
" 改行コードの自動判別。左側から優先。
set fileformats=unix,dos,mac
" □や○の表示対策
set ambiwidth=double


" ---------- タブ・インデントの設定 ----------
" タブ入力時ソフトタブ化する ↓との違いが不明
set expandtab
" タブをスペースに変換する ↑との違いが不明
set smarttab
" タブ文字の幅
set tabstop=4
" 連続した空白に対してタブキーやバックスペースキーでカーソルが動く幅
set softtabstop=4
" 自動でインデントを挿入
set autoindent
" 改行時に前の行の構文をチェックし次の行のインデントを増減する
set smartindent
" smartindentで増減する幅
set shiftwidth=4


" ---------------------------------------------------
" 検索の設定
" ---------------------------------------------------
" インクリメンタルサーチON
set incsearch
" 検索時大文字小文字を無視する
set ignorecase
" 検索時大文字の場合は通常検索
set smartcase
" 検索結果のハイライト
set hlsearch


" ---------------------------------------------------
" カーソル関連の設定
" ---------------------------------------------------
" カーソルの左右移動で行頭行末間が移動可能に
set whichwrap=b,s,h,l,<,>,[,],~
" 下部バーにカーソル位置を表示
set ruler
" 行番号を設定
set number
" 行間の設定
set linespace=4
" 現在行のハイライト
set cursorline
" カーソルラインをなくす(これをしないと不可視文字の色がおかしなことになるので仕方ない)
hi clear Cursorline
" 現在行ハイライト用SpecialKey設定
" autocmd ColorScheme * htmlhighlightskip link MySpecialKey SpecialKey
" autocmd VimEnter,WinEnter * let w:m_sp = matchadd("MySpecialKey", '\(\t\| \+$\)')

" 左右スクロールを許可
" set nowrap

" バックスペースキーの有効化
set backspace=indent,eol,start


" ---------------------------------------------------
" カッコ・タグジャンプ
" ---------------------------------------------------
" カッコの対応関係を一瞬表示する
set showmatch
" vimの「%」でHTMLタグやRubyの記述にも対応可能に
source $VIMRUNTIME/macros/matchit.vim


" ---------------------------------------------------
" コマンドの補完
" ---------------------------------------------------
" コマンドモードの補完
set wildmenu
" コマンドを表示する
set showcmd
" 保存するコマンド履歴の数
set history=100


" ---------------------------------------------------
" クリップボードの設定
" ---------------------------------------------------
"クリップボードの共有
set clipboard=unnamed

" ペースト時のインデント調整
if &term =~ "xterm"
    let &t_SI .= "\e[?2004h"
    let &t_EI .= "\e[?2004l"
    let &pastetoggle = "\e[201~"

    function XTermPasteBegin(ret)
        set paste
        return a:ret
    endfunction

    inoremap <special> <expr> <Esc>[200~ XTermPasteBegin("")
endif


" ---------------------------------------------------
" 不可視文字表示の設定
" ---------------------------------------------------
" 不可視文字の表示
set list

" 不可視文字の表示内容を指定する
" [tab:>-]タブの表示を決定する。値は2文字で指定し、タブがスペース4文字に当たる場合、“>---”と表示される。
" [trail]行末に続くスペースを表す表示。
" [eol]改行記号を表す表示。
" [extends]ウィンドウの幅が狭くて右に省略された文字がある場合に表示される。
" [precedes]extendsと同じで左に省略された文字がある場合に表示される。
" [nbsp:%]不可視のスペースを表す表示。ただし、この記号の通りに表示されるのは“&nbsp;”、つまり、ノーブレークスペースに限らる。
set listchars=tab:￫_,trail:･,eol:$
"改行文字とタブ文字の色設定(NonTextが改行、SpecialKeyがタブ)
hi NonText guibg=NONE guifg=Gray30
hi SpecialKey guibg=NONE guifg=Gray30


" ---------------------------------------------------
" ファイルやバッファに関する設定
" ---------------------------------------------------
"スワップファイルの作成場所の指定
set directory=~/.vim/tmp
"バックアップファイルの作成場所の指定
set backupdir=~/.vim/tmp
"アンドゥファイルの作成場所の指定
set undodir=~/.vim/tmp
"編集中のファイルが変更されたら自動で読み直す
set autoread
"バッファが編集中でもその他のファイルを開けるように
set hidden


" ---------------------------------------------------
" その他設定
" ---------------------------------------------------
" タブを常に表示する
" 0:常に非表示 1:2つ以上タブページがある場合に表示 2:常に表示
set showtabline=2

" 行が折り返し表示されていた場合、行単位ではなく表示行単位でカーソルを移動
nmap j gj
nmap k gk
nmap <down> gj
nmap <up> gk

" Esc連打で検索時のハイライトを解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>
" nmap <C-j><C-j> :nohlsearch<CR><Esc>

" ESCの役割を他のキーに付与
" noremap <C-j> <esc>
" noremap! <C-j> <esc>
inoremap <silent> jj <ESC>

" <Leader>設定
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>tt :tabnew<CR>
nnoremap <Leader>tn gt
nnoremap <Leader>tp gT
nnoremap <Leader>bn :bn<CR>
nnoremap <Leader>bp :bp<CR>
nnoremap <Leader>bd :bd<CR>

